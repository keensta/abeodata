package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagIntArray;

public class ANBTTagIntArray extends NBTTagIntArray{
	
	public ANBTTagIntArray(String s) {
		super(s);
	}
	
	public ANBTTagIntArray(String s, int[] i) {
		super(s, i);
	}

}
