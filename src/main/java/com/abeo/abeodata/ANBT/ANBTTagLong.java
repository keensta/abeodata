package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagLong;

public class ANBTTagLong extends NBTTagLong{
	
	public ANBTTagLong(String s) {
		super(s);
	}
	
	public ANBTTagLong(String s, long l) {
		super(s, l);
	}

}
