package com.abeo.abeodata.ANBT;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.minecraft.server.v1_6_R2.NBTBase;
import net.minecraft.server.v1_6_R2.NBTTagCompound;

public class ANBTTagCompound extends NBTTagCompound{
	
	public ANBTTagCompound() {
		super();
	}
	
	public ANBTTagCompound(String s) {
		super(s);
	}
	
	@SuppressWarnings("rawtypes")
	public static ANBTTagCompound convertNBT(NBTTagCompound n, ANBTTagCompound a) {
		Map map = getNbtMap(n);
		if(map == null)
			return null;
		
	    Iterator iterator = map.keySet().iterator();

        while (iterator.hasNext()) {
            String s = (String) iterator.next();
            
            a.set(s, ((NBTBase) map.get(s)).clone());
        }
		return a;
	}
	
	@SuppressWarnings("rawtypes")
	public static Map getNbtMap(NBTTagCompound n) {
		Map map = new HashMap();
		try {
			Field field = n.getClass().getDeclaredField("map");
			field.setAccessible(true);
			map = (Map) field.get(n);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
}
