package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagInt;

public class ANBTTagInt extends NBTTagInt{
	
	public ANBTTagInt(String s) {
		super(s);
	}
	
	public ANBTTagInt(String s, int i) {
		super(s, i);
	}

}
