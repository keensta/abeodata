package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagFloat;

public class ANBTTagFloat extends NBTTagFloat{
	
	public ANBTTagFloat(String s) {
		super(s);
	}
	
	public ANBTTagFloat(String s, float f) {
		super(s, f);
	}

}
