package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagByteArray;

public class ANBTTagByteArray extends NBTTagByteArray {
	
	public ANBTTagByteArray(String s) {
		super(s);
	}
	
	public ANBTTagByteArray(String s, byte[] b) {
		super(s, b);
	}

}
