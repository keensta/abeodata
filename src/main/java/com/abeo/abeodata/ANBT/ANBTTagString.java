package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagString;

public class ANBTTagString extends NBTTagString {

	public ANBTTagString(String s) {
		super(s);
	}
	
	public ANBTTagString(String s, String s1) {
		super(s, s1);
	}

}
