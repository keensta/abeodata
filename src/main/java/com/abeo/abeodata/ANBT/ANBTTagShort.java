package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagShort;

public class ANBTTagShort extends NBTTagShort{
	
	public ANBTTagShort(String s) {
		super(s);
	}
	
	public ANBTTagShort(String s, short s1) {
		super(s, s1);
	}

}
