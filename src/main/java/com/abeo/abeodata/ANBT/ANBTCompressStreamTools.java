package com.abeo.abeodata.ANBT;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import net.minecraft.server.v1_6_R2.NBTBase;
import net.minecraft.server.v1_6_R2.NBTTagCompound;

public class ANBTCompressStreamTools {

	  public static ANBTTagCompound loadTag(InputStream inputstream) throws IOException {
	        DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new GZIPInputStream(inputstream)));

	        ANBTTagCompound ANBTtagcompound;

	        try {
	            ANBTtagcompound = a((DataInput) datainputstream);
	        } finally {
	           datainputstream.close();
	        }

	        return ANBTtagcompound;
	    }

	    public static void saveTag(ANBTTagCompound ANBTtagcompound, OutputStream outputstream) throws IOException {
	        DataOutputStream dataoutputstream = new DataOutputStream(new GZIPOutputStream(outputstream));

	        try {
	            a(ANBTtagcompound, (DataOutput) dataoutputstream);
	        } finally {
	            dataoutputstream.close();
	        }
	    }

	    public static ANBTTagCompound a(byte[] abyte) throws IOException {
	        DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(abyte))));

	        ANBTTagCompound ANBTtagcompound;

	        try {
	            ANBTtagcompound = a((DataInput) datainputstream);
	        } finally {
	            datainputstream.close();
	        }

	        return ANBTtagcompound;
	    }

	    public static byte[] a(ANBTTagCompound ANBTtagcompound) throws IOException {
	        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
	        DataOutputStream dataoutputstream = new DataOutputStream(new GZIPOutputStream(bytearrayoutputstream));

	        try {
	            a(ANBTtagcompound, (DataOutput) dataoutputstream);
	        } finally {
	            dataoutputstream.close();
	        }

	        return bytearrayoutputstream.toByteArray();
	    }

	    public static ANBTTagCompound a(DataInput datainput) throws IOException {
	        NBTBase nbtbase = NBTBase.a(datainput);

	        if (nbtbase instanceof NBTTagCompound) {
	        	ANBTTagCompound data = new ANBTTagCompound();
	        	data = ANBTTagCompound.convertNBT((NBTTagCompound) nbtbase, data);
	            return data;
	        } else {
	            throw new IOException("Root tag must be a named compound tag");
	        }
	    }

	    public static void a(ANBTTagCompound ANBTtagcompound, DataOutput dataoutput) {
	        NBTBase.a(ANBTtagcompound, dataoutput);
	    }
	
}
