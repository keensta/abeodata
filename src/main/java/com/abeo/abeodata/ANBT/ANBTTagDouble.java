package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagDouble;

public class ANBTTagDouble extends NBTTagDouble{
	
	public ANBTTagDouble(String s) {
		super(s);
	}
	
	public ANBTTagDouble(String s, double d) {
		super(s, d);
	}

}
