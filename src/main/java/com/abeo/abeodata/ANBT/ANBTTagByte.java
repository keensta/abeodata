package com.abeo.abeodata.ANBT;

import net.minecraft.server.v1_6_R2.NBTTagByte;

public class ANBTTagByte extends NBTTagByte{

	public ANBTTagByte(String s) {
		super(s);
	}
	
	public ANBTTagByte(String s, byte b) {
		super(s, b);
	}

}
