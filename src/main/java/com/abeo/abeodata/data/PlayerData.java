package com.abeo.abeodata.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bukkit.entity.Player;

import com.abeo.abeodata.AbeoData;
import com.abeo.abeodata.ANBT.ANBTCompressStreamTools;
import com.abeo.abeodata.ANBT.ANBTTagCompound;

public class PlayerData {

	/**
	 *  Saves the given data to the players Nbt file
	 * @param s Value name you wish to save
	 * @param s1 Value you wish to save converted to string
	 * @param p Player you wish to save the data to
	 * @return true if successful, false otherwise
	 */
	public static boolean saveToPlayer(String s, String s1, Player p) {
		File f = new File(AbeoData.getAbeoData().getDataFolder(), p.getName()+".dat");
		
		ANBTTagCompound tagData = new ANBTTagCompound("playerData");
		tagData.setString(s, s1);
		
		try {
			FileOutputStream out = new FileOutputStream(f);
			ANBTCompressStreamTools.saveTag(tagData, out);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Loads the data you want from the player file
	 * @param s Is the value you want
	 * @param p Player that you want to load the value from
	 * @return returns value as string, Otherwise null if not there
	 */
	public static String loadFromPlayer(String s, Player p) {
		File f = new File(AbeoData.getAbeoData().getDataFolder(), p.getName()+".dat");
		
		if(f.exists()) {
			FileInputStream in;
			try {
				in = new FileInputStream(f);
				try {
					
					ANBTTagCompound data = ANBTCompressStreamTools.loadTag(in);
									
					if(data.hasKey(s)) {
						return data.getString(s);
					} else {
						return null;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
