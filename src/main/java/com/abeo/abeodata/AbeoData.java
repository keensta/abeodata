package com.abeo.abeodata;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class AbeoData extends JavaPlugin {
	
	Logger log = Logger.getLogger("Minecraft");
	
	static AbeoData instance = null;
	
	@Override
	public void onDisable() {
		log.info("[AbeoData] AbeoData disabled");
	}

	@Override
	public void onEnable() {
		instance = this;
		
		log.info("[AbeoData] AbeoData enabled");
	}
	
	public static AbeoData getAbeoData() {
		return instance;
	}
	
	//TODO: Create a replica of NBT data as ANBT this way it becomes Version free. (Still on the ropes about this) Not sure if worth it


}